<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* default/agenda.html.twig */
class __TwigTemplate_09f781485ef37b9fa01afacff1ce7e5ce1e0ad354d8482314bf30a27dcf0a412 extends \Twig\Template
{
    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->blocks = [
            'content' => [$this, 'block_content'],
            'javascripts' => [$this, 'block_javascripts'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "layaut_admin.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "default/agenda.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "default/agenda.html.twig"));

        $this->parent = $this->loadTemplate("layaut_admin.html.twig", "default/agenda.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 2
    public function block_content($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "content"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "content"));

        // line 3
        echo "      <form method=\"post\" id=\"form\" action=\"";
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("crearTarea");
        echo "\">
          <div id=\"body\">

          </div>
      </form>
  ";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 10
    public function block_javascripts($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "javascripts"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "javascripts"));

        // line 11
        echo "    ";
        $this->displayParentBlock("javascripts", $context, $blocks);
        echo "
    <script type=\"text/javascript\">
        \$(document).ready(function () {
            var url = \"";
        // line 14
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("miAgendaAjax");
        echo "\"
            \$.post(url, function (data) {
                \$('#body').html(data);
                \$('#example').DataTable();

                \$('#nuevaTarea').click(function (e) {
                    e.preventDefault();
                    var url = \"";
        // line 21
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("crearTarea");
        echo "\"
                    \$.post(url, function (data) {
                        \$('#body').html(data);
                    });
                });

                \$('.actionModificarTarea').click(function (e) {
                    e.preventDefault();
                    var url = \"";
        // line 29
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("modificarTarea", ["idTarea" => "idTarea"]);
        echo "\";
                    url = url.replace('idTarea', \$(this).data('identificador'));
                    \$.post(url, function (data) {
                        \$('#body').html(data);
                        \$('#form').attr('action', url);
                    });
                });
                \$('.actionEliminarTarea').click(function (e) {
                    e.preventDefault();
                    var url = \"";
        // line 38
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("eliminarTarea", ["idTarea" => "idTarea"]);
        echo "\";

                    url = url.replace('idTarea', \$(this).data('identificador'));
                    var r = confirm(\"¿Esta seguro que desea realizar esta operación?\")
                    if (r == true) {
                        \$.post(url, function (data2) {
                            \$.alert('La operación ha si realizada satisfactoriamente.', {
                                title: 'Mensaje',
                                closeTime: 1500,
                                position: ['top-right'],
                                type: 'success',
                            });
                            setTimeout(function () {
                                    location.href = location.href;
                                },
                                1600
                            );
                        });
                    }
                });
                \$('.actionCompletarTarea').click(function (e) {
                    e.preventDefault();
                    var url = \"";
        // line 60
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("completarTarea", ["idTarea" => "idTarea"]);
        echo "\";
                    url = url.replace('idTarea', \$(this).data('identificador'));
                    var r = confirm(\"¿Esta seguro que desea realizar esta operación?\")
                    if (r == true) {
                        \$.post(url, function (data2) {
                            \$.alert('La operación ha si realizada satisfactoriamente.', {
                                title: 'Mensaje',
                                closeTime: 1500,
                                position: ['top-right'],
                                type: 'success',
                            });
                            setTimeout(function () {
                                    location.href = location.href;
                                },
                                1600
                            );
                        });
                    }
                });
            });
        });
    </script>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "default/agenda.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  152 => 60,  127 => 38,  115 => 29,  104 => 21,  94 => 14,  87 => 11,  78 => 10,  61 => 3,  52 => 2,  30 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Source("{% extends 'layaut_admin.html.twig' %}
  {% block content %}
      <form method=\"post\" id=\"form\" action=\"{{ path('crearTarea') }}\">
          <div id=\"body\">

          </div>
      </form>
  {% endblock %}

{% block javascripts %}
    {{ parent() }}
    <script type=\"text/javascript\">
        \$(document).ready(function () {
            var url = \"{{ path('miAgendaAjax') }}\"
            \$.post(url, function (data) {
                \$('#body').html(data);
                \$('#example').DataTable();

                \$('#nuevaTarea').click(function (e) {
                    e.preventDefault();
                    var url = \"{{ path('crearTarea') }}\"
                    \$.post(url, function (data) {
                        \$('#body').html(data);
                    });
                });

                \$('.actionModificarTarea').click(function (e) {
                    e.preventDefault();
                    var url = \"{{ path('modificarTarea', { 'idTarea' : 'idTarea' }) }}\";
                    url = url.replace('idTarea', \$(this).data('identificador'));
                    \$.post(url, function (data) {
                        \$('#body').html(data);
                        \$('#form').attr('action', url);
                    });
                });
                \$('.actionEliminarTarea').click(function (e) {
                    e.preventDefault();
                    var url = \"{{ path('eliminarTarea', { 'idTarea' : 'idTarea' }) }}\";

                    url = url.replace('idTarea', \$(this).data('identificador'));
                    var r = confirm(\"¿Esta seguro que desea realizar esta operación?\")
                    if (r == true) {
                        \$.post(url, function (data2) {
                            \$.alert('La operación ha si realizada satisfactoriamente.', {
                                title: 'Mensaje',
                                closeTime: 1500,
                                position: ['top-right'],
                                type: 'success',
                            });
                            setTimeout(function () {
                                    location.href = location.href;
                                },
                                1600
                            );
                        });
                    }
                });
                \$('.actionCompletarTarea').click(function (e) {
                    e.preventDefault();
                    var url = \"{{ path('completarTarea', { 'idTarea' : 'idTarea' }) }}\";
                    url = url.replace('idTarea', \$(this).data('identificador'));
                    var r = confirm(\"¿Esta seguro que desea realizar esta operación?\")
                    if (r == true) {
                        \$.post(url, function (data2) {
                            \$.alert('La operación ha si realizada satisfactoriamente.', {
                                title: 'Mensaje',
                                closeTime: 1500,
                                position: ['top-right'],
                                type: 'success',
                            });
                            setTimeout(function () {
                                    location.href = location.href;
                                },
                                1600
                            );
                        });
                    }
                });
            });
        });
    </script>
{% endblock %}

", "default/agenda.html.twig", "/var/www/html/symfony3/app/Resources/views/default/agenda.html.twig");
    }
}
