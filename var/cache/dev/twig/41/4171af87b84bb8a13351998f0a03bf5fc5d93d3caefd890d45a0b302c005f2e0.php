<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* default/agenda_ajax.html.twig */
class __TwigTemplate_d0eaf79b5765fdc8e898f1789e5c12003dbb5a47a8388ed77f6a2b1b46432777 extends \Twig\Template
{
    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "default/agenda_ajax.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "default/agenda_ajax.html.twig"));

        // line 1
        echo "<br>
<div class=\"row\">
    <div class=\"col-md-12\">
        <a id=\"nuevaTarea\" class=\"btn btn-primary float-md-right\">Crear tarea</a>
    </div>
</div>
<br>
<div class=\"row\">
    <div class=\"col-md-12\">
        <h5>Mi agenda</h5>
        <hr>
    </div>
</div>
";
        // line 14
        if ((twig_length_filter($this->env, ($context["tareas"] ?? $this->getContext($context, "tareas"))) > 0)) {
            // line 15
            echo "    <table id=\"example\" class=\"table table-striped table-bordered\" style=\"width:100%\">
        <thead>
        <tr>
            <th>N.</th>
            <th>Nombre</th>
            <th>Descripción</th>
            <th>Fecha de creación</th>
            <th>Fecha límite</th>
            <th>Completada</th>
            <th>Categoría</th>
            <th>Opciones</th>
        </tr>
        </thead>
        <tbody>
        ";
            // line 29
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["tareas"] ?? $this->getContext($context, "tareas")));
            foreach ($context['_seq'] as $context["element"] => $context["item"]) {
                // line 30
                echo "            ";
                $context["id"] = $this->getAttribute($context["item"], "id", []);
                // line 31
                echo "            <tr id=\"tr_";
                echo twig_escape_filter($this->env, $this->getAttribute($context["item"], "id", []), "html", null, true);
                echo "\">
                <td>";
                // line 32
                echo twig_escape_filter($this->env, ($context["element"] + 1), "html", null, true);
                echo "</td>
                <td>";
                // line 33
                echo twig_escape_filter($this->env, (($this->getAttribute($context["item"], "nombre", [], "any", true, true)) ? (_twig_default_filter($this->getAttribute($context["item"], "nombre", []), "-")) : ("-")), "html", null, true);
                echo "</td>
                <td>";
                // line 34
                echo twig_escape_filter($this->env, (($this->getAttribute($context["item"], "descripcion", [], "any", true, true)) ? (_twig_default_filter($this->getAttribute($context["item"], "descripcion", []), "-")) : ("-")), "html", null, true);
                echo "</td>
                <td>";
                // line 35
                echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($context["item"], "fechaCreacion", []), "d/m/Y"), "html", null, true);
                echo "</td>
                <td>";
                // line 36
                echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($context["item"], "fechaLimite", []), "d/m/Y"), "html", null, true);
                echo "</td>
                <td>";
                // line 37
                echo (($this->getAttribute($context["item"], "completada", [])) ? ("Si") : ((((twig_date_format_filter($this->env, "now", "Y-m-d") > twig_date_format_filter($this->env, $this->getAttribute($context["item"], "fechaLimite", []), "Y-m-d"))) ? ("No") : ("Si"))));
                // line 38
                echo " </td>
                <td>";
                // line 39
                echo twig_escape_filter($this->env, (($this->getAttribute($this->getAttribute($context["item"], "idCategoria", [], "any", false, true), "nombre", [], "any", true, true)) ? (_twig_default_filter($this->getAttribute($this->getAttribute($context["item"], "idCategoria", [], "any", false, true), "nombre", []), "-")) : ("-")), "html", null, true);
                echo "</td>
                <td>
                    <a class=\"actionModificarTarea\" data-identificador=\"";
                // line 41
                echo twig_escape_filter($this->env, ($context["id"] ?? $this->getContext($context, "id")), "html", null, true);
                echo "\" title=\"Modificar tarea\"><i
                                class=\"fa fa-edit\"></i></a>
                    <a class=\"actionEliminarTarea\" data-identificador=\"";
                // line 43
                echo twig_escape_filter($this->env, ($context["id"] ?? $this->getContext($context, "id")), "html", null, true);
                echo "\" title=\"Eliminar tarea\"><i
                                class=\"fa fa-trash\"></i></a>
                    <a class=\"actionCompletarTarea\" data-identificador=\"";
                // line 45
                echo twig_escape_filter($this->env, ($context["id"] ?? $this->getContext($context, "id")), "html", null, true);
                echo "\" title=\"Completar tarea\"><i
                                class=\"fa fa-toggle-off\"></i></a>
                </td>
            </tr>
        ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['element'], $context['item'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 50
            echo "        </tbody>
    </table>
";
        } else {
            // line 53
            echo "    No existen registros que mostrar
";
        }
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "default/agenda_ajax.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  137 => 53,  132 => 50,  121 => 45,  116 => 43,  111 => 41,  106 => 39,  103 => 38,  101 => 37,  97 => 36,  93 => 35,  89 => 34,  85 => 33,  81 => 32,  76 => 31,  73 => 30,  69 => 29,  53 => 15,  51 => 14,  36 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Source("<br>
<div class=\"row\">
    <div class=\"col-md-12\">
        <a id=\"nuevaTarea\" class=\"btn btn-primary float-md-right\">Crear tarea</a>
    </div>
</div>
<br>
<div class=\"row\">
    <div class=\"col-md-12\">
        <h5>Mi agenda</h5>
        <hr>
    </div>
</div>
{% if tareas | length > 0 %}
    <table id=\"example\" class=\"table table-striped table-bordered\" style=\"width:100%\">
        <thead>
        <tr>
            <th>N.</th>
            <th>Nombre</th>
            <th>Descripción</th>
            <th>Fecha de creación</th>
            <th>Fecha límite</th>
            <th>Completada</th>
            <th>Categoría</th>
            <th>Opciones</th>
        </tr>
        </thead>
        <tbody>
        {% for element,item in tareas %}
            {% set id = item.id %}
            <tr id=\"tr_{{ item.id }}\">
                <td>{{ element+1 }}</td>
                <td>{{ item.nombre | default('-') }}</td>
                <td>{{ item.descripcion | default('-') }}</td>
                <td>{{ item.fechaCreacion | date('d/m/Y') }}</td>
                <td>{{ item.fechaLimite | date('d/m/Y') }}</td>
                <td>{{ item.completada ? 'Si': (\"now\"|date('Y-m-d') > item.fechaLimite| date('Y-m-d')  ?
                    'No' : 'Si') }} </td>
                <td>{{ item.idCategoria.nombre | default('-') }}</td>
                <td>
                    <a class=\"actionModificarTarea\" data-identificador=\"{{ id }}\" title=\"Modificar tarea\"><i
                                class=\"fa fa-edit\"></i></a>
                    <a class=\"actionEliminarTarea\" data-identificador=\"{{ id }}\" title=\"Eliminar tarea\"><i
                                class=\"fa fa-trash\"></i></a>
                    <a class=\"actionCompletarTarea\" data-identificador=\"{{ id }}\" title=\"Completar tarea\"><i
                                class=\"fa fa-toggle-off\"></i></a>
                </td>
            </tr>
        {% endfor %}
        </tbody>
    </table>
{% else %}
    No existen registros que mostrar
{% endif %}
", "default/agenda_ajax.html.twig", "/var/www/html/symfony3/app/Resources/views/default/agenda_ajax.html.twig");
    }
}
