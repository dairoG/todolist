<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* default/agenda.html.twig */
class __TwigTemplate_910d0806913cac6bafe2f957794177d43a62f3bddef978d6ff7255299ec744e0 extends \Twig\Template
{
    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->blocks = [
            'content' => [$this, 'block_content'],
            'javascripts' => [$this, 'block_javascripts'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "layaut_admin.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $this->parent = $this->loadTemplate("layaut_admin.html.twig", "default/agenda.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_content($context, array $blocks = [])
    {
        // line 3
        echo "      <div id=\"body\">

      </div>
  ";
    }

    // line 8
    public function block_javascripts($context, array $blocks = [])
    {
        // line 9
        echo "    ";
        $this->displayParentBlock("javascripts", $context, $blocks);
        echo "
    <script type=\"text/javascript\">
        \$(document).ready(function () {
            var url = \"";
        // line 12
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("miAgendaAjax");
        echo "\"
            \$.post(url, function (data) {
                \$('#body').html(data);
                \$('#example').DataTable();

                \$('#nuevaTarea').click(function (e) {
                    e.preventDefault();
                    var url = \"";
        // line 19
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("crearTarea");
        echo "\"
                    \$.post(url, function (data) {
                        \$('#body').html(data);
                    });
                });

                \$('.actionModificarTarea').click(function (e) {
                    e.preventDefault();
                    var url = \"";
        // line 27
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("modificarTarea", ["idTarea" => "idTarea"]);
        echo "\";
                    url = url.replace('idTarea', \$(this).data('identificador'));
                    \$.post(url, function (data) {
                        \$('#body').html(data);
                    });
                });


                \$('.actionEliminarTarea').click(function (e) {
                    e.preventDefault();
                    var url = \"";
        // line 37
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("eliminarTarea", ["idTarea" => "idTarea"]);
        echo "\";

                    url = url.replace('idTarea', \$(this).data('identificador'));
                    var r = confirm(\"Desea eliminar el elemento\")
                    if (r == true) {
                        \$.post(url, function (data2) {
                            // alert(\"tr_\" +data2.data );
                            \$(\"tr_\" + data2.data).remove();
                            location.href = location.href;
                            // alert(data2.message);
                        });
                    }
                });
                \$('.actionCompletarTarea').click(function (e) {
                    e.preventDefault();
                    var url = \"";
        // line 52
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("completarTarea", ["idTarea" => "idTarea"]);
        echo "\";
                    url = url.replace('idTarea', \$(this).data('identificador'));
                    var r = confirm(\"Desea realizar la operacion\")
                    if (r == true) {
                        \$.post(url, function (data2) {
                            location.href = location.href;
                            alert(data2.message);
                        });
                    }
                });

            });
        });
    </script>
";
    }

    public function getTemplateName()
    {
        return "default/agenda.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  112 => 52,  94 => 37,  81 => 27,  70 => 19,  60 => 12,  53 => 9,  50 => 8,  43 => 3,  40 => 2,  30 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Source("", "default/agenda.html.twig", "/var/www/html/symfony3/app/Resources/views/default/agenda.html.twig");
    }
}
