<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* default/agenda_ajax.html.twig */
class __TwigTemplate_bea478359bd08ee647ac6a1ce738d06e8a29e9df66f5b9b845c322bcc268ded7 extends \Twig\Template
{
    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 1
        echo "<div class=\"row rounded-right\" >
    <a id=\"nuevaTarea\" class=\"btn btn-primary\">Crear tarea</a>
</div>
<br>
<div class=\"row\">
    <div class=\"col-md-12\">
        <h5>Mi agenda</h5>
        <hr>
    </div>
</div>
";
        // line 11
        if ((twig_length_filter($this->env, ($context["tareas"] ?? null)) > 0)) {
            // line 12
            echo "    <table id=\"example\" class=\"table table-striped table-bordered\" style=\"width:100%\">
        <thead>
        <tr>
            <th>N.</th>
            <th>Nombre</th>
            <th>Descripción</th>
            <th>Fecha de creación</th>
            <th>Fecha límite</th>
            <th>Completada</th>
            <th>Categoría</th>
            <th>Opciones</th>
        </tr>
        </thead>
        <tbody>
        ";
            // line 26
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["tareas"] ?? null));
            foreach ($context['_seq'] as $context["element"] => $context["item"]) {
                // line 27
                echo "            ";
                $context["id"] = $this->getAttribute($context["item"], "id", []);
                // line 28
                echo "            <tr id=\"tr_";
                echo twig_escape_filter($this->env, $this->getAttribute($context["item"], "id", []), "html", null, true);
                echo "\">
                <td>";
                // line 29
                echo twig_escape_filter($this->env, ($context["element"] + 1), "html", null, true);
                echo "</td>
                <td>";
                // line 30
                echo twig_escape_filter($this->env, (($this->getAttribute($context["item"], "nombre", [], "any", true, true)) ? (_twig_default_filter($this->getAttribute($context["item"], "nombre", []), "-")) : ("-")), "html", null, true);
                echo "</td>
                <td>";
                // line 31
                echo twig_escape_filter($this->env, (($this->getAttribute($context["item"], "descripcion", [], "any", true, true)) ? (_twig_default_filter($this->getAttribute($context["item"], "descripcion", []), "-")) : ("-")), "html", null, true);
                echo "</td>
                <td>";
                // line 32
                echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($context["item"], "fechaCreacion", []), "d/m/Y"), "html", null, true);
                echo "</td>
                <td>";
                // line 33
                echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($context["item"], "fechaLimite", []), "d/m/Y"), "html", null, true);
                echo "</td>
                <td>";
                // line 34
                echo (($this->getAttribute($context["item"], "completada", [])) ? ("Si") : ((((twig_date_format_filter($this->env, "now", "Y-m-d") > twig_date_format_filter($this->env, $this->getAttribute($context["item"], "fechaLimite", []), "Y-m-d"))) ? ("No") : ("Si"))));
                // line 35
                echo " </td>
                <td>";
                // line 36
                echo twig_escape_filter($this->env, (($this->getAttribute($this->getAttribute($context["item"], "idCategoria", [], "any", false, true), "nombre", [], "any", true, true)) ? (_twig_default_filter($this->getAttribute($this->getAttribute($context["item"], "idCategoria", [], "any", false, true), "nombre", []), "-")) : ("-")), "html", null, true);
                echo "</td>
                <td>
                    <a class=\"actionModificarTarea\" data-identificador=\"";
                // line 38
                echo twig_escape_filter($this->env, ($context["id"] ?? null), "html", null, true);
                echo "\" title=\"Modificar tarea\"><i class=\"fa fa-edit\"></i></a>
                    <a class=\"actionEliminarTarea\" data-identificador=\"";
                // line 39
                echo twig_escape_filter($this->env, ($context["id"] ?? null), "html", null, true);
                echo "\" title=\"Eliminar tarea\"><i class=\"fa fa-trash\"></i></a>
                    <a class=\"actionCompletarTarea\" data-identificador=\"";
                // line 40
                echo twig_escape_filter($this->env, ($context["id"] ?? null), "html", null, true);
                echo "\" title=\"Completar tarea\"><i class=\"fa fa-toggle-off\"></i></a>
                </td>
            </tr>
        ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['element'], $context['item'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 44
            echo "        </tbody>
    </table>
";
        } else {
            // line 47
            echo "    No existen registros que mostrar
";
        }
    }

    public function getTemplateName()
    {
        return "default/agenda_ajax.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  125 => 47,  120 => 44,  110 => 40,  106 => 39,  102 => 38,  97 => 36,  94 => 35,  92 => 34,  88 => 33,  84 => 32,  80 => 31,  76 => 30,  72 => 29,  67 => 28,  64 => 27,  60 => 26,  44 => 12,  42 => 11,  30 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Source("", "default/agenda_ajax.html.twig", "/var/www/html/symfony3/app/Resources/views/default/agenda_ajax.html.twig");
    }
}
