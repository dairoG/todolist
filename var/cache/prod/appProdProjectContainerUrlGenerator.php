<?php

use Symfony\Component\Routing\RequestContext;
use Symfony\Component\Routing\Exception\RouteNotFoundException;
use Psr\Log\LoggerInterface;

/**
 * This class has been auto-generated
 * by the Symfony Routing Component.
 */
class appProdProjectContainerUrlGenerator extends Symfony\Component\Routing\Generator\UrlGenerator
{
    private static $declaredRoutes;

    public function __construct(RequestContext $context, LoggerInterface $logger = null)
    {
        $this->context = $context;
        $this->logger = $logger;
        if (null === self::$declaredRoutes) {
            self::$declaredRoutes = [
        'miAgenda' => array (  0 =>   array (  ),  1 =>   array (    '_controller' => 'AppBundle\\Controller\\ApiController::miAgendaAction',  ),  2 =>   array (  ),  3 =>   array (    0 =>     array (      0 => 'text',      1 => '/miAgenda',    ),  ),  4 =>   array (  ),  5 =>   array (  ),),
        'miAgendaAjax' => array (  0 =>   array (  ),  1 =>   array (    '_controller' => 'AppBundle\\Controller\\ApiController::miAgendaAjaxAction',  ),  2 =>   array (  ),  3 =>   array (    0 =>     array (      0 => 'text',      1 => '/miAgendaAjax',    ),  ),  4 =>   array (  ),  5 =>   array (  ),),
        'crearTarea' => array (  0 =>   array (  ),  1 =>   array (    '_controller' => 'AppBundle\\Controller\\ApiController::crearTareaAction',  ),  2 =>   array (  ),  3 =>   array (    0 =>     array (      0 => 'text',      1 => '/crearTarea',    ),  ),  4 =>   array (  ),  5 =>   array (  ),),
        'modificarTarea' => array (  0 =>   array (    0 => 'idTarea',  ),  1 =>   array (    '_controller' => 'AppBundle\\Controller\\ApiController::modificarTareaAction',  ),  2 =>   array (  ),  3 =>   array (    0 =>     array (      0 => 'variable',      1 => '/',      2 => '[^/]++',      3 => 'idTarea',    ),    1 =>     array (      0 => 'text',      1 => '/modificarTarea',    ),  ),  4 =>   array (  ),  5 =>   array (  ),),
        'eliminarTarea' => array (  0 =>   array (    0 => 'idTarea',  ),  1 =>   array (    '_controller' => 'AppBundle\\Controller\\ApiController::eliminarTareaAction',  ),  2 =>   array (  ),  3 =>   array (    0 =>     array (      0 => 'variable',      1 => '/',      2 => '[^/]++',      3 => 'idTarea',    ),    1 =>     array (      0 => 'text',      1 => '/eliminarTarea',    ),  ),  4 =>   array (  ),  5 =>   array (  ),),
        'completarTarea' => array (  0 =>   array (    0 => 'idTarea',  ),  1 =>   array (    '_controller' => 'AppBundle\\Controller\\ApiController::completarTareaAction',  ),  2 =>   array (  ),  3 =>   array (    0 =>     array (      0 => 'variable',      1 => '/',      2 => '[^/]++',      3 => 'idTarea',    ),    1 =>     array (      0 => 'text',      1 => '/completarTarea',    ),  ),  4 =>   array (  ),  5 =>   array (  ),),
        'homepage' => array (  0 =>   array (  ),  1 =>   array (    '_controller' => 'AppBundle\\Controller\\DefaultController::indexAction',  ),  2 =>   array (  ),  3 =>   array (    0 =>     array (      0 => 'text',      1 => '/',    ),  ),  4 =>   array (  ),  5 =>   array (  ),),
    ];
        }
    }

    public function generate($name, $parameters = [], $referenceType = self::ABSOLUTE_PATH)
    {
        if (!isset(self::$declaredRoutes[$name])) {
            throw new RouteNotFoundException(sprintf('Unable to generate a URL for the named route "%s" as such route does not exist.', $name));
        }

        list($variables, $defaults, $requirements, $tokens, $hostTokens, $requiredSchemes) = self::$declaredRoutes[$name];

        return $this->doGenerate($variables, $defaults, $requirements, $tokens, $parameters, $name, $referenceType, $hostTokens, $requiredSchemes);
    }
}
