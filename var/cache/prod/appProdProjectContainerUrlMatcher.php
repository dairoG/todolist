<?php

use Symfony\Component\Routing\Exception\MethodNotAllowedException;
use Symfony\Component\Routing\Exception\ResourceNotFoundException;
use Symfony\Component\Routing\RequestContext;

/**
 * This class has been auto-generated
 * by the Symfony Routing Component.
 */
class appProdProjectContainerUrlMatcher extends Symfony\Bundle\FrameworkBundle\Routing\RedirectableUrlMatcher
{
    public function __construct(RequestContext $context)
    {
        $this->context = $context;
    }

    public function match($rawPathinfo)
    {
        $allow = [];
        $pathinfo = rawurldecode($rawPathinfo);
        $trimmedPathinfo = rtrim($pathinfo, '/');
        $context = $this->context;
        $request = $this->request ?: $this->createRequest($pathinfo);
        $requestMethod = $canonicalMethod = $context->getMethod();

        if ('HEAD' === $requestMethod) {
            $canonicalMethod = 'GET';
        }

        if (0 === strpos($pathinfo, '/miAgenda')) {
            // miAgenda
            if ('/miAgenda' === $pathinfo) {
                return array (  '_controller' => 'AppBundle\\Controller\\ApiController::miAgendaAction',  '_route' => 'miAgenda',);
            }

            // miAgendaAjax
            if ('/miAgendaAjax' === $pathinfo) {
                return array (  '_controller' => 'AppBundle\\Controller\\ApiController::miAgendaAjaxAction',  '_route' => 'miAgendaAjax',);
            }

        }

        // modificarTarea
        if (0 === strpos($pathinfo, '/modificarTarea') && preg_match('#^/modificarTarea/(?P<idTarea>[^/]++)$#sD', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, ['_route' => 'modificarTarea']), array (  '_controller' => 'AppBundle\\Controller\\ApiController::modificarTareaAction',));
        }

        // crearTarea
        if ('/crearTarea' === $pathinfo) {
            return array (  '_controller' => 'AppBundle\\Controller\\ApiController::crearTareaAction',  '_route' => 'crearTarea',);
        }

        // completarTarea
        if (0 === strpos($pathinfo, '/completarTarea') && preg_match('#^/completarTarea/(?P<idTarea>[^/]++)$#sD', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, ['_route' => 'completarTarea']), array (  '_controller' => 'AppBundle\\Controller\\ApiController::completarTareaAction',));
        }

        // eliminarTarea
        if (0 === strpos($pathinfo, '/eliminarTarea') && preg_match('#^/eliminarTarea/(?P<idTarea>[^/]++)$#sD', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, ['_route' => 'eliminarTarea']), array (  '_controller' => 'AppBundle\\Controller\\ApiController::eliminarTareaAction',));
        }

        // homepage
        if ('' === $trimmedPathinfo) {
            $ret = array (  '_controller' => 'AppBundle\\Controller\\DefaultController::indexAction',  '_route' => 'homepage',);
            if ('/' === substr($pathinfo, -1)) {
                // no-op
            } elseif ('GET' !== $canonicalMethod) {
                goto not_homepage;
            } else {
                return array_replace($ret, $this->redirect($rawPathinfo.'/', 'homepage'));
            }

            return $ret;
        }
        not_homepage:

        if ('/' === $pathinfo && !$allow) {
            throw new Symfony\Component\Routing\Exception\NoConfigurationException();
        }

        throw 0 < count($allow) ? new MethodNotAllowedException(array_unique($allow)) : new ResourceNotFoundException();
    }
}
