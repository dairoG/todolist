<?php

namespace AppBundle\Controller;

use App\JsonConvert\Utils;
use AppBundle\Entity\TareaEntity;
use AppBundle\Form\TareaEntityType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\BrowserKit\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class ApiController extends Controller
{
    /**
     * @Route("/miAgenda", name="miAgenda")
     */
    public function miAgendaAction(Request $request, EntityManagerInterface $entityManager)
    {
       return $this->render('default/agenda.html.twig');
    }
    /**
     * @Route("/miAgendaAjax", name="miAgendaAjax")
     */
    public function miAgendaAjaxAction(Request $request, EntityManagerInterface $entityManager)
    {
        $params = $entityManager->getRepository('AppBundle:TareaEntity')->findBy(
            [], ['id' => 'DESC']
        );
        return $this->render('default/agenda_ajax.html.twig', ['tareas' => $params]);
    }

    /**
     * @Route("/crearTarea", name="crearTarea")
     */
    public function crearTareaAction(Request $request, EntityManagerInterface $entityManager)
    {
        $post = $request->request->all();
        $nuevaTarea = new TareaEntity();
        $form = $this->createForm(TareaEntityType::class, $nuevaTarea);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->persist($nuevaTarea);
            $entityManager->flush();
            return $this->redirectToRoute('miAgenda');
        }
        return $this->render('default/registrar_tarea.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/modificarTarea/{idTarea}", name="modificarTarea")
     */
    public function modificarTareaAction(TareaEntity $idTarea, Request $request, EntityManagerInterface $entityManager)
    {
        $form = $this->createForm(TareaEntityType::class, $idTarea);
        $form->handleRequest($request);
        $post = $request->request->all();
        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->persist($idTarea);
            $entityManager->flush();
            return $this->redirectToRoute('miAgenda');
        }
        return $this->render('default/modificar_tarea.html.twig', [
            'tarea' => $idTarea,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/eliminarTarea/{idTarea}", name="eliminarTarea")
     */
    public function eliminarTareaAction(TareaEntity $idTarea, Request $request, EntityManagerInterface $entityManager)
    {
        $tempId = $idTarea->getId();
        try {
            $entityManager->remove($idTarea);
            $entityManager->flush();
            $params = [
                'code' => 200,
                'message' => 'La acción se ha realizado satisfactoriamente',
                'data' => $tempId
            ];
            echo json_encode($params);
            die;
        } catch (\Exception $exc) {
            $params = [
                'code' => 403,
                'message' => 'Ha ocurrido un error al realizar la operación.',
                'data' => null
            ];
        }
        echo json_encode($params);
        die;
    }


    /**
     * @Route("/completarTarea/{idTarea}", name="completarTarea")
     */
    public function completarTareaAction(TareaEntity $idTarea, Request $request, EntityManagerInterface $entityManager)
    {
        try {
            $idTarea->setCompletada(true);
            $entityManager->flush();
            $params = [
                'code' => 200,
                'message' => 'La acción se ha realizado satisfactoriamente',
                'data' => null
            ];
            echo json_encode($params);
            die;
        } catch (\Exception $exc) {
            $params = [
                'code' => 403,
                'message' => 'Ha ocurrido un error al realizar la operación.',
                'data' => null
            ];
        }
        echo json_encode($params);
        die;
    }

}
