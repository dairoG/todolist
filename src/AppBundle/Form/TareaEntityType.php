<?php

namespace AppBundle\Form;

use AppBundle\Entity\CategoriaEntity;
use AppBundle\Repository\CategoriaEntityRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class TareaEntityType extends AbstractType
{
    private $em;

    /**
     * TareaEntityType constructor.
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->em = $entityManager;
    }


    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder
            ->add('nombre', TextType::class, array("required" => "required", "attr" => array(
                "class" => "form-control",
            )))
            ->add('descripcion', TextType::class, array("required" => false, "attr" => array(
                "class" => "form-control",
            )))
            ->add('fechaLimite', DateType::class, array('widget' => 'single_text', "required" => "required", "attr" => array(
                "class" => "form-control",
            )))
            ->add('idCategoria', EntityType::class, array(
                'label' => 'Categoría',
                'placeholder' => '--Seleccione--',
                'class' => CategoriaEntity::class,
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('u')
                        ->where('u.activo = true')
                        ->orderBy('u.nombre', 'ASC');
                },
                "attr" => array(
                    "class" => "form-control select2"
                )
            ));
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\TareaEntity'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_tareaentity';
    }


}
